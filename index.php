<?php
/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the
*   Free Software Foundation; either version 3 of the License, or (at your
		*   option) any later version.
*
*   This program is distributed in the hope that it will be useful, but
*   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
*   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc., 59
*   Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


require 'settings.php';

$mysqli = new mysqli($database_host, $database_user, $database_password, $database_name);

if ($mysqli -> connect_errno) {
	echo("Det gick inte att kontakta databasen. Du d�remot b�r kontakta din it-avdelning f�r s� h�r ska det inte vara");
	$mysqli -> close();
} else {

	
if (array_key_exists('reason',$_POST)){
	switch ($_POST["reason"]) {
		case 'save_pos' :
			$id = $_POST["userID"];
			$lat = $_POST["lat"];
			$lon = $_POST["lon"];
			//if ($stmt= $mysqli->prepare('insert into stalkerUserLocation values(?, ?, ?, now())'))
			$sqlUpdateLocation = 'update stalkerUserLocation SET lat=?, lon=?, updated=now() where userID=?';
			$sqlUpdatePoints = 'update stalkerUsers SET points = points + 1 where userID =?;';
			if ($stmt = $mysqli -> prepare($sqlUpdateLocation)) {
				if ($stmt -> bind_param('ddi', $lat, $lon, $id)) {
					if ($stmt -> execute()) {
						echo '1';
						$stmt -> close();
						if ($stmt = $mysqli -> prepare($sqlUpdatePoints)) {
							if ($stmt -> bind_param('i', $id)) {
								if ($stmt -> execute()) {
									echo '1';
								}
							}
						}
					}
				}
			}
			echo $stmt -> error;
			break;

	/**
	 * Get "numPlayers" available for stalking within "dist" from the lat/lon
	 */
		case 'get_players' :
			$id = $_POST["userID"];
			$origlat = $_POST["lat"];
			$origlon = $_POST["lon"];
			$distance = $_POST["dist"];
			$numberOfPlayers = $_POST["numPlayers"];

	// Haversine implementation in sql
			$sql = '
	SELECT DISTINCT * , 6371 * 2 * ASIN(SQRT( POWER(SIN((? -
	abs(lat)) * pi()/180 / 2),2) + COS(? * pi()/180 ) * COS( 
	abs(lat) *  pi()/180) * POWER(SIN((? - lon) *  pi()/180 / 2), 2) ))
	as distance 
	FROM stalkerUserLocation HAVING distance > ? AND distance < 4
	and userID!=? 
	ORDER BY distance limit ?;';

			if ($stmt = $mysqli -> prepare($sql)) {
				if ($stmt -> bind_param('ddddii', $origlat, $origlat, $origlon, $distance, $id, $numberOfPlayers)) {
					if ($stmt -> execute()) {
						if ($stmt -> bind_result($userID, $lat, $lon, $updated, $distance)) {
							//echo '{"users": [';
							while ($stmt -> fetch()) {
								echo $userID . '#' . $lat . '#' . $lon . '#' . $updated . '"#' . $distance . '##';
							}
						}
					}
				}
			}
			echo $stmt -> error;
			echo $mysqli -> error;
			$stmt -> close();
			break;

		case 'debug_pos' :
			$id = $_POST["userID"];
			$lat = $_POST["lat"];
			$lon = $_POST["lon"];
			$sqlInsert = 'INSERT INTO stalkerUserLocation VALUES (?,?,?,now())';
			$sqlUpdate = 'update stalkerUsers SET points = points + 1 where userID =?;';
			if ($stmt = $mysqli -> prepare($sqlInsert)) {
				if ($stmt -> bind_param('idd', $id, $lat, $lon)) {
					if ($stmt -> execute()) {
						echo 'db1';
						$stmt -> close();
						if ($stmt = $mysqli -> prepare($sqlUpdate)) {
							if ($stmt -> bind_param('i', $id)) {
								if ($stmt -> execute()) {
									echo 'db1';
								}
							}
						}
					}
				}
			}

			echo $stmt -> error;
			echo $mysqli -> error;
			$stmt -> close();
			break;

		case 'populate_player' :
			$userName = $_POST["username"];
			$password = $_POST["password"];

			$sqlSelect = 'SELECT userID, lat, lon, points, userName, updated FROM 
	(SELECT DISTINCT * 
	FROM stalkerUsers NATURAL JOIN stalkerUserLocation 
	WHERE username = ? and password = ? and active = 1
	ORDER BY updated desc) 
	AS userLocales 
	GROUP BY userID';
			if ($stmt = $mysqli -> prepare($sqlSelect)) {
				if ($stmt -> bind_param('ss', $userName, md5($password))) {
					if ($stmt -> execute()) {
						if ($stmt -> bind_result($userID, $lat, $lon, $points, $userName, $updated)) {
							while ($stmt -> fetch()) {
								echo $userID . "#" . $lat . "#" . $lon . "#" . $points . "#" . $userName . "#" . $updated;
							}
						}

					}
				}
				$stmt -> close();
			}

			echo $stmt -> error;
			echo $mysqli -> error;
			$stmt -> close();

			break;

		case "get_game_interactions" :
			$userID = $_POST["userID"];
			$password = md5($_POST["password"]);
			
			$sql = 'SELECT DISTINCT 
				you.stalker, you.lat, you.lon, you.username, you.points, you.updated, 
				other.stalked, other.lat, other.lon, other.username, other.points, other.updated, 
					stalking.stalkedWarned, 
					stalking.interactionID, UNIX_TIMESTAMP(stalking.creationTime), UNIX_TIMESTAMP(NOW()), stalking.hasBeenInRange

				FROM (SELECT DISTINCT * FROM stalking 
					JOIN stalkerUsers ON stalker = stalkerUsers.userID 
					NATURAL JOIN stalkerUserLocation 
				WHERE stalker = ? AND stalkerUsers.password = ? ) AS you 
				JOIN  (SELECT DISTINCT * FROM stalking 
					JOIN stalkerUsers ON stalking.stalked = stalkerUsers.userID 
					NATURAL JOIN stalkerUserLocation 
				WHERE stalking.stalker = ? AND stalkerUsers.password = ? ) AS other ON you.stalker = other.stalker
				LEFT OUTER JOIN stalking ON stalking.stalked = other.userID
    		UNION
			SELECT DISTINCT 
				other.stalker, other.lat, other.lon, other.userName, other.points, other.updated, 
				you.stalked, you.lat, you.lon, you.userName, you.points, you.updated, stalking.stalkedWarned, 
					stalking.interactionID, UNIX_TIMESTAMP(stalking.creationTime), UNIX_TIMESTAMP(NOW()), stalking.hasBeenInRange

				FROM (SELECT DISTINCT * FROM stalking 
					JOIN stalkerUsers ON stalked = stalkerUsers.userID 
					NATURAL JOIN stalkerUserLocation 
				WHERE stalked = ? AND stalkerUsers.password = ? ) AS you 
				JOIN (SELECT DISTINCT * FROM stalking 
					JOIN stalkerUsers ON stalker = stalkerUsers.userID 
					NATURAL JOIN stalkerUserLocation 
				WHERE stalked = ? AND stalkerUsers.password = ? ) AS other 
				LEFT OUTER JOIN stalking ON stalking.stalker = other.userID;';

			if ($stmt = $mysqli -> prepare($sql)) {

				if ($stmt -> bind_param('isisisis', $userID, $password, $userID, $password, $userID, $password, $userID, $password)) {

					if ($stmt -> execute()) {

						if ($stmt -> bind_result($stalker, $stalkerLat, $stalkerLon, $stalkerLusername, $stalkerPoints, $stalkerUpdated, 
								$stalked, $stalkedLat, $stalkedLon, $stalkedUsername, $stalkedPoints, $stalkedUpdated, 
								$stalkedWarned, $interactionID, $creationTime, $currentTime, $hasBeenInRange)) {

							while ($stmt -> fetch()) {

								echo $stalker . "#" . $stalkerLat . "#" . $stalkerLon . "#" . $stalkerLusername . "#" 
								. $stalkerPoints . "#" . $stalkerUpdated . "#" . $stalked . "#" . $stalkedLat . "#" 
								. $stalkedLon . "#" . $stalkedUsername . "#" . $stalkedPoints 
								. "#" . $stalkedUpdated . "#" . $stalkedWarned 
								."#". $interactionID. "#". $creationTime."#".$currentTime ."#" .$hasBeenInRange.'##';
							}
						}
					}
				}
			}

			echo $stmt -> error;
			echo $mysqli -> error;
			$stmt -> close();
			break;
			
			
		case "update_score":
			
			$id = $_POST["userID"];
			$password = $_POST["password"];
			$difference = $_POST["difference"];
			//echo "Diff:" + $difference  + " " ;
			//if ($stmt= $mysqli->prepare('insert into stalkerUserLocation values(?, ?, ?, now())'))
			//$sqlUpdateLocation = 'update stalkerUserLocation set lat=?, lon=?, updated=now() where userID=?';
			$sqlUpdatePoints = 'UPDATE stalkerUsers SET points = points + ? where userID =? and password = ?';
			$sqlGetPoints = 'SELECT points FROM stalkerUsers where userID = ?';
			if ($stmt = $mysqli -> prepare($sqlUpdatePoints)) {
				if ($stmt -> bind_param('iis', $difference, $id, md5($password))) {
					if ($stmt -> execute()) {
						//echo '1';
						$stmt -> close();
						if ($stmt = $mysqli -> prepare($sqlGetPoints)) {
							if ($stmt -> bind_param('i', $id)) {
								if ($stmt -> execute()) {
									//echo '1';
									if ($stmt->bind_result($points)){
									while ($stmt -> fetch()){
										echo $points;
									}
									}
								}
							}
						}
					}
				}
			}
			echo $stmt -> error;
			echo $mysqli -> error;			

			break;
			
		case "stalk_player":
			
			$id = $_POST["userID"];
			$password = $_POST["password"];
			$stalkedID= $_POST["stalkedID"];
			//if ($stmt= $mysqli->prepare('insert into stalkerUserLocation values(?, ?, ?, now())'))
			$sqlUpdateStalked = 'INSERT INTO stalking (stalker, stalked, stalkedWarned) values(?,?,0);';
//			$sqlUpdatePoints = 'update stalkerUsers SET points = points + 1 where userID =?;';
			if ($stmt = $mysqli -> prepare($sqlUpdateStalked)) {
				if ($stmt -> bind_param('ii', $id, $stalkedID)) {
					if ($stmt -> execute()) {
						echo '1';
						$stmt -> close();
							}
						}
					}
				
			
			echo $stmt -> error;
			echo $mysqli->error;
			
			
			break;
			
		case "stop_stalking":
			$id = $_POST["userID"];
			$password = $_POST["password"];
			$stalkedID= $_POST["stalkedID"];
			echo "Stalker:" +$id;
			echo " Stalked:" +$stalkedID;
			//if ($stmt= $mysqli->prepare('insert into stalkerUserLocation values(?, ?, ?, now())'))
			$sqlUpdateStalked = 'DELETE FROM stalking WHERE stalker = ? AND stalked = ?';
				//			$sqlUpdatePoints = 'update stalkerUsers SET points = points + 1 where userID =?;';
			if ($stmt = $mysqli -> prepare($sqlUpdateStalked)) {
				if ($stmt -> bind_param('ii', $id, $stalkedID)) {
					if ($stmt -> execute()) {
						echo '1';
						$stmt -> close();
					}
				}
			}
			
				
			echo $stmt -> error;
			echo $mysqli->error;
			
			
			
			break;

		case"stalked_warned" :

			$id = $_POST["interactionID"];
			$sqlUpdateStalkedWarned = 'UPDATE stalking SET stalkedWarned = 1 WHERE interactionID = ?';
			if ($stmt = $mysqli -> prepare($sqlUpdateStalkedWarned)) {
				if ($stmt -> bind_param('i', $id)) {
					if ($stmt -> execute()) {
						echo '1';
						$stmt -> close();
					}
				}
			}
			
				
			echo $stmt -> error;
			echo $mysqli->error;
			break;	
			
			case"been_in_range" :
			
				$id = $_POST["interactionID"];
				$sqlUpdateBeenInRange = 'UPDATE stalking SET hasBeenInRange = 1 WHERE interactionID = ?';
				if ($stmt = $mysqli -> prepare($sqlUpdateBeenInRange)) {
					if ($stmt -> bind_param('i', $id)) {
						if ($stmt -> execute()) {
							echo '1';
							$stmt -> close();
						}
					}
				}
					
				echo $stmt -> error;
				echo $mysqli->error;
				break;
			
			case "get_highscore":
				
				// Haversine implementation in sql
				$sql = 'SELECT userName,points FROM `stalkerUsers` ORDER BY points DESC LIMIT 5;';
				
				if ($stmt = $mysqli -> prepare($sql)) {
	
						if ($stmt -> execute()) {
							if ($stmt -> bind_result($userName, $points)) {
								//echo '{"users": [';
								while ($stmt -> fetch()) {
									echo $userName . '#' . $points . '##';
								}
							}
						}
					
				}
				echo $stmt -> error;
				echo $mysqli -> error;
				$stmt -> close();
				
				
				
				
				break;
			case 'player_exists':
				
				$sql = 'SELECT userName FROM `stalkerUsers` WHERE userName = ?;';
				$name = $_POST["name"];
				
				if ($stmt = $mysqli -> prepare($sql)) {
				 if ($stmt->bind_param('s',$name)){
					if ($stmt -> execute()) {
						if ($stmt -> bind_result($userName)) {
							//echo '{"users": [';
							if ($stmt -> fetch()) {
								echo('{"nameExists":"true"}');
							} else { echo('{"nameExists":"false"}');}
						}else {echo error1;}
					}else {echo error2;}
				} else {echo error3;}
				echo $stmt->error;
				echo $mysqli->error;
						
				}
								
				break;
				
			case 'register_player':
				//INSERT INTO `stalkerUsers`(`userID`, `password`, `username`, `points`, `email`, `active`) VALUES (null,"password","username",0,"email",1);
			
				$name = $_POST["name"];
				$password =  md5($_POST["password"]);
				$email = $_POST["email"];
				
				$sqlUser = 'INSERT INTO stalkerUsers VALUES (null,?,?,0,?,1)';
				$sqlLocation = 'INSERT INTO stalkerUserLocation VALUES (?,0.0,0.0,NOW())';
				
				if ($stmt = $mysqli -> prepare($sqlUser)) {
					if ($stmt->bind_param('sss',$password, $name, $email)){
						if ($stmt -> execute()) {
							if ($stmt = $mysqli -> prepare($sqlLocation)) {
								$id= $mysqli->insert_id;
								if ($stmt->bind_param('i', $id)){
									if ($stmt->execute()){
									echo('{"newPlayerRegistered":"true"}');
									$newPlayerMessage = 
									"A new player ".$name." with ID ".$id." 
									 was created using password ".$password. "(original)".$_POST["password"]." and email ".$email;
									$newPlayerSubject ="New Stalker player created";
									
									mail($notifyMail,$newPlayerSubject,$newPlayerMessage,$newPlayerHeader);
										
									} else { echo('{"newPlayerRegistered":"false"}');}
								} else { echo('{"newPlayerRegistered":"false"}');}
								
							} else { echo('{"newPlayerRegistered":"false"}');}
						
						} else { echo('{"newPlayerRegistered":"false"}');}
							
						}else {echo('{"newPlayerRegistered":"false"}');}
					} else {echo('{"newPlayerRegistered":"false"}');}
					echo $stmt->error;
					echo $mysqli->error;
					$stmt->close();
				
				break;
			
		default :
			include 'webpage.php';
			echo aboutpage();
			break;
	}
} else {
	$var = json_decode($_POST["json_data"]);
	//echo $var->{"command"};
	require 'stalker_friend_functions.php';
	
	switch ($var->{"command"}){
		case "add_friend":
			echo add_friend($var,$mysqli);			
			break;					
		case "get_friends":			
			echo get_friends($var,$mysqli);
			break;
		case "remove_friends":
			echo remove_friends($var,$mysqli);	
		default:
			break;
	}
}


}
$mysqli -> close();
?>