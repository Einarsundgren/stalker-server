<?php 
function add_friend($json,$mysqli){

	$ret ="";
	$user1id = $json->{"user1id"};
	$user2id =  $json->{"user2id"};
	$sqlUser = 'INSERT INTO StalkerIsFriends VALUES (?,?)';
	
	if ($stmt = $mysqli -> prepare($sqlUser)) {
		if ($stmt->bind_param('ii',$user1id, $user2id)){
			if ($stmt -> execute()) {
				$ret='{"is_friends":"true"}';
			} else { $ret='{"is_friends":"false"}';}
		}else {$ret='{"is_friends":"false"}';}
	} else {$ret='{"is_friends":"false"}';}
	echo $stmt->error;
	echo $mysqli->error;
	$stmt->close();
	return $ret;
}	
	
	
function get_friends($json, $mysqli){
	$sql='select distinct stalkerUsers.userID,username,points,lat,lon,updated from stalkerUsers join StalkerIsFriends on userID = user2ID join stalkerUserLocation on stalkerUserLocation.userID=user2ID where user1ID=? and active =1;
';
	
	$userID = $json->{"user_id"};
	//echo "ID: ".$userID;
	if ($stmt = $mysqli -> prepare($sql)) {
	
		if ($stmt -> bind_param('i', $userID)) {
	
			if ($stmt -> execute()) {
	
				if ($stmt -> bind_result($userID, $userName, $points, $lat, $lon, $updated)) {
					$ret='{"friends": [';
	
					while ($stmt -> fetch()) {
	
						$ret.= '{"name":"'.$userName . '","user_id":"' . $userID . '","lat":"' . $lat . '","lon":"' 
								. $lon . '","points":"'	. $points . '","updated":"' . $updated . '"},';
					}

					//Just remove the last  ","
					$ret = substr_replace($ret ,"",-1);
					
					$ret.="]}";
				} else { $ret='{"failed_operation":"true"}';}
			} else { $ret='{"failed_operation":"true"}';}
		} else { $ret='{"failed_operation":"true"}';}
	} else { $ret='{"failed_operation":"true"}';}
	
	echo $stmt -> error;
	echo $mysqli -> error;
	$stmt -> close();
	return $ret;
	
	
	
}

?>